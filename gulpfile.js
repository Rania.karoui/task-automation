// On déclare nos dépendances gulp
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();


// Sass utilised le compiler dde nodejs
sass.compiler = require('node-sass');

// Création de ma tâche pour compiler mes fichiers sass en css
gulp.task('sass', function (){
    return gulp.src('./src/sass/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(autoprefixer())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./build/css'))
    .pipe(browserSync.stream());
});

gulp.task('html', function(){
	return gulp.src('./src/**/*.html')
	.pipe(gulp.dest('./build'))
});

// On crée un watcher pour surveiller les changements sur les fichiers
gulp.task('watch', function () {
    browserSync.init({
        server: {
            baseDir: './build'
        }
    })
    gulp.watch('./src/sass/**/*.scss', gulp.series('sass'));
    gulp.watch("./src/*.html", gulp.series('html')).on('change', browserSync.reload);

  });

  gulp.task('default', gulp.parallel('sass', 'html', 'watch'));

